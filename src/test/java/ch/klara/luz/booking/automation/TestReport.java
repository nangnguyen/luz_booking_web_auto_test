package ch.klara.luz.booking.automation;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestReport {

	private static final String TEST_SERVER_DOMAIN = "http://klara-local.axonivy.io";
	private static String username = System.getenv("BROWSERSTACK_USERNAME");
	private static String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
	private static String buildName = System.getenv("BROWSERSTACK_BUILD_NAME");
	private static String browserstackLocal = System.getenv("BROWSERSTACK_LOCAL");
	private static String browserstackLocalIdentifier = System.getenv("BROWSERSTACK_LOCAL_IDENTIFIER");
	private static final String URL = "https://" + username + ":" + accessKey + "@hub-cloud.browserstack.com/wd/hub";
	private WebDriver driver;
	private DesiredCapabilities caps;
	private JavascriptExecutor javascriptExecutor;
	
	@Before
	public void init() throws MalformedURLException {
	    caps = new DesiredCapabilities();
	    caps.setCapability("os_version", "10");
	    caps.setCapability("resolution", "1920x1080");
	    caps.setCapability("browser", "Chrome");
	    caps.setCapability("browser_version", "latest");
	    caps.setCapability("os", "Windows");
	    caps.setCapability("build", "Configuration Page Testing"); // CI/CD job or build name
	    caps.setCapability("build", buildName);
	    caps.setCapability("browserstack.video", "true");
	    caps.setCapability("browserstack.local", browserstackLocal);
	    caps.setCapability("browserstack.localIdentifier", browserstackLocalIdentifier);
	}
	

	@Rule
    public TestRule watcher = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
        	try {
				File srcFile = (File) ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(srcFile, new File(System.getProperty("user.dir") + "/reports/browser-stack/screenshot.png"));
				String scriptToMarkResultFailed = "browserstack_executor: "
						+ "{"
						+ "\"action\": \"setSessionStatus\", "
						+ "\"arguments\": {"
						+ "\"status\": \"failed\", "
						+ "\"reason\": "
						+ "\"" + e.getMessage() + "\""
						+ "}}";
				javascriptExecutor.executeScript(scriptToMarkResultFailed);
				driver.quit();
			} catch (IOException e2) {
				e2.printStackTrace();
			}
        }
        
        @Override
	    protected void succeeded(Description description) {
				String scriptToMarkResultPassed = "browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\": \"passed\", \"reason\": \"No error!\"}}";
				javascriptExecutor.executeScript(scriptToMarkResultPassed);
				driver.quit();
	    }
    };
    
	@Test
	public void createAndDragResourceToPool_successful() throws IOException, InterruptedException {
	    caps.setCapability("name", "Create and drag resource to pool successfully"); // test name
	    driver = new RemoteWebDriver(new java.net.URL(URL), caps);
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
	    javascriptExecutor = (JavascriptExecutor)driver;
	    driver.get(TEST_SERVER_DOMAIN);

		WebElement userName = driver.findElement(By.id("username"));
		userName.sendKeys("khoa.tang@axonactive.com");

		WebElement passWord = driver.findElement(By.id("password"));
		passWord.sendKeys("KhoaTP12345@");
		passWord.submit();

		WebElement selectedCompany = driver.findElement(By.cssSelector("button[id$='selectCompanyButton']"));
		selectedCompany.click();

		WebElement sidebarAnchor = driver.findElement(By.cssSelector(".sidebar-anchor"));
		sidebarAnchor.click();

		javascriptExecutor.executeScript("$('.sidebar-scroll-content').mCustomScrollbar('scrollTo','bottom')");

		WebElement onlineBookingMenu = driver.findElement(By.cssSelector("[id$='subMenuComponents-booking2'] a"));
		onlineBookingMenu.click();

		WebElement configurationMenu = driver.findElement(By.cssSelector("li[id$='menuItem-configuration'] a"));
		configurationMenu.click();

		/**
		 * Create person
		 */
		
		WebDriverWait waitForSpinnerAppear = new WebDriverWait(driver, 30);
		waitForSpinnerAppear
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='person-area']//button")));
		Thread.sleep(500);
		driver.findElement(By.xpath("//div[@id='person-area']//button")).click();

		By freelancer = By.xpath("//div[contains(@id,'person-form-dialog')]//div[contains(@id,'person-type')]//div[2]");
		waitForSpinnerAppear.until(ExpectedConditions.visibilityOfElementLocated(freelancer));
		waitForSpinnerAppear.until(ExpectedConditions.elementToBeClickable(freelancer));
		driver.findElement(freelancer).click();

		Thread.sleep(1000);
		By firstName = By.xpath("//input[contains(@id, 'firstName')]");
		waitForSpinnerAppear.until(ExpectedConditions.visibilityOfElementLocated(firstName));
		driver.findElement(firstName).sendKeys("Thuong");

		WebElement lastName = driver.findElement(By.xpath("//input[contains(@id, 'lastName')]"));
		lastName.sendKeys("Vo");

		WebElement nickNameFreelancer = driver.findElement(By.xpath("//input[contains(@id, 'nickNameFreelancer')]"));
		nickNameFreelancer.sendKeys("Jon");

		WebElement email = driver.findElement(By.xpath("//input[contains(@id, 'email')]"));
		email.sendKeys("a@gmail.com");

		WebElement firstStore = driver.findElement(By.xpath(
				"//div[contains(@id, 'person-dialog-content-wrapper')]//div[@class='col-9']//div[contains(@id,'booleanToggleSwitch')]"));
		firstStore.click();

		Thread.sleep(2000);
		WebElement saveBtn = driver.findElement(By.xpath("//button[contains(@id, 'create-person')]"));
		System.out.println(saveBtn);
		saveBtn.click();
		Thread.sleep(500);

		WebElement newResource = driver.findElement(By.xpath("//span[text()='Jon']"));
		System.out.println(newResource);

		/**
		 * drag and drop
		 */
		By resourceSelector = By.xpath("//div[@id='person-list']/div[contains(@class, 'resource-card')]//i[contains(@class, 'st-drag-handle')]");
		WebElement resource = webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(resourceSelector));
		System.out.println(resource.getAttribute("class"));
		WebElement resourcePoolAssignment = driver.findElement(By.xpath("//div[@id='resource-pool-list']//div[contains(@class, 'resource-assignment-list')]"));
		System.out.println(resourcePoolAssignment.getAttribute("class"));
		
		Actions act = new Actions(driver);
		act.click(resource).build().perform();
		Thread.sleep(1000);
		act.clickAndHold(resource).build().perform();
		Thread.sleep(1000);
		act.moveToElement(resourcePoolAssignment).build().perform();
		Thread.sleep(1000);
		act.release(resourcePoolAssignment).build().perform();
		Thread.sleep(1000);
	}
	
	@Test
	public void login_successfully() throws IOException {
		caps.setCapability("name", "Login Failed"); // test name
		driver = new RemoteWebDriver(new java.net.URL(URL), caps);
		driver.manage().window().maximize();
		driver.get(TEST_SERVER_DOMAIN);
		javascriptExecutor = (JavascriptExecutor)driver;

		WebElement userName = driver.findElement(By.id("username"));
		userName.sendKeys("khoa.tang@axonactive.com");

		WebElement passWord = driver.findElement(By.id("password"));
		passWord.sendKeys("KhoaTP12345");
		passWord.submit();
		
		Assert.assertEquals("KLARA", driver.getTitle());
	}
}
